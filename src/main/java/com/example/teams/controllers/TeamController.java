package com.example.teams.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.teams.etities.Team;
import com.example.teams.repositories.TeamRepository;

@RestController
public class TeamController {
	private final TeamRepository repository;

	public TeamController(TeamRepository repository) {
		this.repository = repository;
	}
	
	@GetMapping("/teams")
	public List<Team> getTeams() {
		return repository.findAll();
	}
	
	@PostMapping("/teams")
	public Team saveTeam(@RequestBody Team team) {
		return repository.save(team);
	}
	

}
