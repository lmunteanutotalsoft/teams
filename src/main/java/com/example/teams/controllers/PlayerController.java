package com.example.teams.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.example.teams.etities.Player;
import com.example.teams.etities.Team;
import com.example.teams.repositories.PlayerRepository;
import com.example.teams.repositories.TeamRepository;

@RestController
public class PlayerController {

	private PlayerRepository playerPepository;
	
	private TeamRepository teamRepository;
	
	public PlayerController(PlayerRepository playerPepository, TeamRepository teamRepository) {
		super();
		this.playerPepository = playerPepository;
		this.teamRepository = teamRepository;
	}

	@GetMapping("/teams/{teamId}/players")
	public List<Player> getPlayers(@PathVariable Long teamId) {
		return playerPepository.findByTeamId(teamId);
	}
	
	@PostMapping("/teams/{teamId}/players")
	public List<Player> savePlayer(@RequestBody List<Player> players, @PathVariable Long teamId) {
		Optional<Team> result = teamRepository.findById(teamId);
		if(!result.isPresent()) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Could not find team " + teamId);
		}
		for(Player player : players) {
			player.setTeam(result.get());
			playerPepository.save(player);
		}
		return players;
	}
}
