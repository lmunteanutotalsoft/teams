package com.example.teams.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.teams.etities.Team;

public interface TeamRepository extends JpaRepository<Team, Long> {

}
