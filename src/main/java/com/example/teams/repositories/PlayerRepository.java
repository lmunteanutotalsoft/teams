package com.example.teams.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.teams.etities.Player;

public interface PlayerRepository extends JpaRepository<Player, Long> {
	public List<Player> findByTeamId(Long teamId);
}
